#include "shell.h"

/**
* main - Entry point to awesome shell
* @argc: Number of argunmebnt
* @argv: Arguments to passed
* Return: 0 if success
*/

int main(int argc, char *argv[])
{
	char *userInput = NULL;
	char **command = NULL;
	int exitStatus = 0;
	(void)argc;
	(void)argv;

	while (true)
	{
		if (isatty(0))
			_print("$ ");
		userInput = getUserInput();
		if (userInput == NULL)   /* C-d */
		{
			if (isatty(0))
				_print("\n");
			return (exitStatus);
		}

		command = tokenizer(userInput);
		if(!command)
			continue;
		for(int i = 0; command[i]; i++)
		{
			_print(command[i]);
			_print("\n");
			free(command[i]), command[i] = NULL;
		}
		free(command), command = NULL;

		/*free userInput to avoid memory leak because getline allocate memory*/
	}

	return (0);
}
