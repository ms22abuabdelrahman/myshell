#ifndef SHELL_H
#define SHELL_H

#define SEP " \t\n"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>

void _print(const char *string);
char *getUserInput(void);
char **tokenizer(char *userInput);


#endif 
